import { Link, routes } from '@redwoodjs/router'

import Teachers from 'src/components/Teacher/Teachers'

export const QUERY = gql`
  query FindTeachers {
    teachers {
      id
      email
      firstName
      lastName
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No teachers yet. '}
      <Link to={routes.newTeacher()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ teachers }) => {
  return <Teachers teachers={teachers} />
}
