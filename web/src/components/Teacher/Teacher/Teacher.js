import humanize from 'humanize-string'

import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'
import { Link, routes, navigate } from '@redwoodjs/router'

const DELETE_TEACHER_MUTATION = gql`
  mutation DeleteTeacherMutation($id: Int!) {
    deleteTeacher(id: $id) {
      id
    }
  }
`

const formatEnum = (values) => {
  if (values) {
    if (Array.isArray(values)) {
      const humanizedValues = values.map((value) => humanize(value))
      return humanizedValues.join(', ')
    } else {
      return humanize(values)
    }
  }
}

const jsonDisplay = (obj) => {
  return (
    <pre>
      <code>{JSON.stringify(obj, null, 2)}</code>
    </pre>
  )
}

const timeTag = (datetime) => {
  return (
    datetime && (
      <time dateTime={datetime} title={datetime}>
        {new Date(datetime).toUTCString()}
      </time>
    )
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const Teacher = ({ teacher }) => {
  const [deleteTeacher] = useMutation(DELETE_TEACHER_MUTATION, {
    onCompleted: () => {
      toast.success('Teacher deleted')
      navigate(routes.teachers())
    },
    onError: (error) => {
      toast.error(error.message)
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete teacher ' + id + '?')) {
      deleteTeacher({ variables: { id } })
    }
  }

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            Teacher {teacher.id} Detail
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>Id</th>
              <td>{teacher.id}</td>
            </tr>
            <tr>
              <th>Email</th>
              <td>{teacher.email}</td>
            </tr>
            <tr>
              <th>First name</th>
              <td>{teacher.firstName}</td>
            </tr>
            <tr>
              <th>Last name</th>
              <td>{teacher.lastName}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <nav className="rw-button-group">
        <Link
          to={routes.editTeacher({ id: teacher.id })}
          className="rw-button rw-button-blue"
        >
          Edit
        </Link>
        <button
          type="button"
          className="rw-button rw-button-red"
          onClick={() => onDeleteClick(teacher.id)}
        >
          Delete
        </button>
      </nav>
    </>
  )
}

export default Teacher
