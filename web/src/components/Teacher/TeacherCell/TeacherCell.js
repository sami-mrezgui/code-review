import Teacher from 'src/components/Teacher/Teacher'

export const QUERY = gql`
  query FindTeacherById($id: Int!) {
    teacher: teacher(id: $id) {
      id
      email
      firstName
      lastName
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Teacher not found</div>

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ teacher }) => {
  return <Teacher teacher={teacher} />
}
