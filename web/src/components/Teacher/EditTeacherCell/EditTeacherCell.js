import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'
import { navigate, routes } from '@redwoodjs/router'

import TeacherForm from 'src/components/Teacher/TeacherForm'

export const QUERY = gql`
  query EditTeacherById($id: Int!) {
    teacher: teacher(id: $id) {
      id
      email
      firstName
      lastName
    }
  }
`
const UPDATE_TEACHER_MUTATION = gql`
  mutation UpdateTeacherMutation($id: Int!, $input: UpdateTeacherInput!) {
    updateTeacher(id: $id, input: $input) {
      id
      email
      firstName
      lastName
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ teacher }) => {
  const [updateTeacher, { loading, error }] = useMutation(
    UPDATE_TEACHER_MUTATION,
    {
      onCompleted: () => {
        toast.success('Teacher updated')
        navigate(routes.teachers())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (input, id) => {
    updateTeacher({ variables: { id, input } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          Edit Teacher {teacher.id}
        </h2>
      </header>
      <div className="rw-segment-main">
        <TeacherForm
          teacher={teacher}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
