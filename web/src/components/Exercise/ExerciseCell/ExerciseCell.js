import Exercise from 'src/components/Exercise/Exercise'

export const QUERY = gql`
  query FindExerciseById($id: Int!) {
    exercise: exercise(id: $id) {
      id
      title
      content
      language
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Exercise not found</div>

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ exercise }) => {
  return <Exercise exercise={exercise} />
}
