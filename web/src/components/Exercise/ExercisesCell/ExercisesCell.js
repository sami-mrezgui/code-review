import { Link, routes } from '@redwoodjs/router'

import Exercises from 'src/components/Exercise/Exercises'

export const QUERY = gql`
  query FindExercises {
    exercises {
      id
      title
      content
      language
      teacherId
      Teacher {
        id
        email
      }
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No exercises yet. '}
      <Link to={routes.newExercise()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ exercises }) => {
  return <Exercises exercises={exercises} />
}
