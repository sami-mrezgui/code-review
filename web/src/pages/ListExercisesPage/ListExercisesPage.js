 import { MetaTags } from '@redwoodjs/web'
import ExercisesCell from 'src/components/Exercise/ExercisesCell'

const ListExercisesPage = () => {
  return (
    <>
      <MetaTags title="ListExercises" description="ListExercises page" />

      <h1>List of Exercises</h1>

      <ExercisesCell></ExercisesCell>
    </>
  )
}

export default ListExercisesPage
