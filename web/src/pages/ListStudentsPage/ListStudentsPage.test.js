import { render } from '@redwoodjs/testing/web'

import ListStudentsPage from './ListStudentsPage'

//   Improve this test with help from the Redwood Testing Doc:
//   https://redwoodjs.com/docs/testing#testing-pages-layouts

describe('ListStudentsPage', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<ListStudentsPage />)
    }).not.toThrow()
  })
})
