 import { MetaTags } from '@redwoodjs/web'
import StudentsCell from 'src/components/Student/StudentsCell'

const ListStudentsPage = () => {
  return (
    <>
      <MetaTags title="ListStudents" description="ListStudents page" />

      <h1>ListStudentsPage</h1>

    <StudentsCell></StudentsCell>
    </>
  )
}

export default ListStudentsPage
