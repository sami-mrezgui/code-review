 import { MetaTags } from '@redwoodjs/web'
import TeachersCell from 'src/components/Teacher/TeachersCell'

const ListTeachersPage = () => {
  return (
    <>
      <MetaTags title="ListTeachers" description="ListTeachers page" />

      <h1>ListTeachersPage</h1>

    <TeachersCell></TeachersCell>
    </>
  )
}

export default ListTeachersPage
