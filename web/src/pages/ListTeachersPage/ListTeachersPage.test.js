import { render } from '@redwoodjs/testing/web'

import ListTeachersPage from './ListTeachersPage'

//   Improve this test with help from the Redwood Testing Doc:
//   https://redwoodjs.com/docs/testing#testing-pages-layouts

describe('ListTeachersPage', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<ListTeachersPage />)
    }).not.toThrow()
  })
})
