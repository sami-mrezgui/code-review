import { Link, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'

const HomePage = () => {
  return (
    <>
      <MetaTags title="Home" description="Home page" />

      <h1>Dashboard</h1>

      <p>
        <Link to={routes.listStudents()}><h3>List of students</h3></Link>
      </p>
      <p>
        <Link to={routes.listExercises()}><h3>List of exercises</h3></Link>
      </p>
      <p>
        <Link to={routes.listTeachers()}><h3>List of teachers</h3></Link>
      </p>
    </>
  )
}

export default HomePage
