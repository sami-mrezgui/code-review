import TeacherCell from 'src/components/Teacher/TeacherCell'

const TeacherPage = ({ id }) => {
  return <TeacherCell id={id} />
}

export default TeacherPage
