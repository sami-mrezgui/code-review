import EditTeacherCell from 'src/components/Teacher/EditTeacherCell'

const EditTeacherPage = ({ id }) => {
  return <EditTeacherCell id={id} />
}

export default EditTeacherPage
