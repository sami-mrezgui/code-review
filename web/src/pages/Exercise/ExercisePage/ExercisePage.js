import ExerciseCell from 'src/components/Exercise/ExerciseCell'

const ExercisePage = ({ id }) => {
  return <ExerciseCell id={id} />
}

export default ExercisePage
