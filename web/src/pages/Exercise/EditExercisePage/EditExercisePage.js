import EditExerciseCell from 'src/components/Exercise/EditExerciseCell'

const EditExercisePage = ({ id }) => {
  return <EditExerciseCell id={id} />
}

export default EditExercisePage
