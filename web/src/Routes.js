// In this file, all Page components from 'src/pages` are auto-imported. Nested
// directories are supported, and should be uppercase. Each subdirectory will be
// prepended onto the component name.
//
// Examples:
//
// 'src/pages/HomePage/HomePage.js'         -> HomePage
// 'src/pages/Admin/BooksPage/BooksPage.js' -> AdminBooksPage

import { Set, Router, Route } from '@redwoodjs/router'
import ExercisesLayout from 'src/layouts/ExercisesLayout'
import StudentsLayout from 'src/layouts/StudentsLayout'
import TeachersLayout from 'src/layouts/TeachersLayout'

const Routes = () => {
  return (
    <Router>
      <Route path="/list-teachers" page={ListTeachersPage} name="listTeachers" />
      <Route path="/list-exercises" page={ListExercisesPage} name="listExercises" />
      <Route path="/list-students" page={ListStudentsPage} name="listStudents" />
      <Route path="/home" page={HomePage} name="home" />
      <Set wrap={ExercisesLayout}>
        <Route path="/exercises/new" page={ExerciseNewExercisePage} name="newExercise" />
        <Route path="/exercises/{id:Int}/edit" page={ExerciseEditExercisePage} name="editExercise" />
        <Route path="/exercises/{id:Int}" page={ExerciseExercisePage} name="exercise" />
        <Route path="/exercises" page={ExerciseExercisesPage} name="exercises" />
      </Set>
      <Set wrap={StudentsLayout}>
        <Route path="/students/new" page={StudentNewStudentPage} name="newStudent" />
        <Route path="/students/{id:Int}/edit" page={StudentEditStudentPage} name="editStudent" />
        <Route path="/students/{id:Int}" page={StudentStudentPage} name="student" />
        <Route path="/students" page={StudentStudentsPage} name="students" />
      </Set>
      <Set wrap={TeachersLayout}>
        <Route path="/teachers/new" page={TeacherNewTeacherPage} name="newTeacher" />
        <Route path="/teachers/{id:Int}/edit" page={TeacherEditTeacherPage} name="editTeacher" />
        <Route path="/teachers/{id:Int}" page={TeacherTeacherPage} name="teacher" />
        <Route path="/teachers" page={TeacherTeachersPage} name="teachers" />
      </Set>
      <Route notfound page={NotFoundPage} />
    </Router>
  )
}

export default Routes
