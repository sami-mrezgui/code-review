import {
  teachers,
  teacher,
  createTeacher,
  updateTeacher,
  deleteTeacher,
} from './teachers'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float and DateTime types.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('teachers', () => {
  scenario('returns all teachers', async (scenario) => {
    const result = await teachers()

    expect(result.length).toEqual(Object.keys(scenario.teacher).length)
  })

  scenario('returns a single teacher', async (scenario) => {
    const result = await teacher({ id: scenario.teacher.one.id })

    expect(result).toEqual(scenario.teacher.one)
  })

  scenario('creates a teacher', async () => {
    const result = await createTeacher({
      input: {
        email: 'String9887548',
        firstName: 'String',
        lastName: 'String',
      },
    })

    expect(result.email).toEqual('String9887548')
    expect(result.firstName).toEqual('String')
    expect(result.lastName).toEqual('String')
  })

  scenario('updates a teacher', async (scenario) => {
    const original = await teacher({ id: scenario.teacher.one.id })
    const result = await updateTeacher({
      id: original.id,
      input: { email: 'String37215282' },
    })

    expect(result.email).toEqual('String37215282')
  })

  scenario('deletes a teacher', async (scenario) => {
    const original = await deleteTeacher({ id: scenario.teacher.one.id })
    const result = await teacher({ id: original.id })

    expect(result).toEqual(null)
  })
})
