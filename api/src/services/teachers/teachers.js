import { db } from 'src/lib/db'

export const teachers = () => {
  return db.teacher.findMany()
}

export const teacher = ({ id }) => {
  return db.teacher.findUnique({
    where: { id },
  })
}

export const createTeacher = ({ input }) => {
  return db.teacher.create({
    data: input,
  })
}

export const updateTeacher = ({ id, input }) => {
  return db.teacher.update({
    data: input,
    where: { id },
  })
}

export const deleteTeacher = ({ id }) => {
  return db.teacher.delete({
    where: { id },
  })
}

export const Teacher = {
  exercises: (_obj, { root }) =>
    db.teacher.findUnique({ where: { id: root.id } }).exercises(),
}
