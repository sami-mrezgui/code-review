export const schema = gql`
  type Teacher {
    id: Int!
    email: String!
    firstName: String!
    lastName: String!
    exercises: [Exercise]!
  }

  type Query {
    teachers: [Teacher!]! @requireAuth
    teacher(id: Int!): Teacher @requireAuth
  }

  input CreateTeacherInput {
    email: String!
    firstName: String!
    lastName: String!
  }

  input UpdateTeacherInput {
    email: String
    firstName: String
    lastName: String
  }

  type Mutation {
    createTeacher(input: CreateTeacherInput!): Teacher! @requireAuth
    updateTeacher(id: Int!, input: UpdateTeacherInput!): Teacher! @requireAuth
    deleteTeacher(id: Int!): Teacher! @requireAuth
  }
`
