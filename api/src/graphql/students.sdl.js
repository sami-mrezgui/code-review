export const schema = gql`
  type Student {
    id: Int!
    email: String!
    firstName: String!
    lastName: String!
  }

  type Query {
    students: [Student!]! @requireAuth
    student(id: Int!): Student @requireAuth
  }

  input CreateStudentInput {
    email: String!
    firstName: String!
    lastName: String!
  }

  input UpdateStudentInput {
    email: String
    firstName: String
    lastName: String
  }

  type Mutation {
    createStudent(input: CreateStudentInput!): Student! @requireAuth
    updateStudent(id: Int!, input: UpdateStudentInput!): Student! @requireAuth
    deleteStudent(id: Int!): Student! @requireAuth
  }
`
