export const schema = gql`
  type Exercise {
    id: Int!
    title: String!
    content: String!
    language: String!
    Teacher: Teacher
    teacherId: Int
  }

  type Query {
    exercises: [Exercise!]! @requireAuth
    exercise(id: Int!): Exercise @requireAuth
  }

  input CreateExerciseInput {
    title: String!
    content: String!
    language: String!
    teacherId: Int
  }

  input UpdateExerciseInput {
    title: String
    content: String
    language: String
    teacherId: Int
  }

  type Mutation {
    createExercise(input: CreateExerciseInput!): Exercise! @requireAuth
    updateExercise(id: Int!, input: UpdateExerciseInput!): Exercise!
      @requireAuth
    deleteExercise(id: Int!): Exercise! @requireAuth
  }
`
